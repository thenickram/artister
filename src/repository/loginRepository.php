<?php

require_once 'repository.php';
require_once  __DIR__.'/../models/login.php';

class loginRepository extends repository
{
    private function generateString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 200; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function setCookies($email)
    {
        $stmt=$this->database->connect()->prepare('
        select * from logged_users where email=:email');
        $stmt->bindParam(':email',$email,PDO::PARAM_STR);
        $stmt->execute();
        $user=$stmt->fetch(PDO::FETCH_ASSOC);
        $code = $this->generateString();
        if($user!=null)
        {
            $update=$this->database->connect()->prepare('
            update logged_users set code=:code where email=:email'
            );
            $update->bindParam(':code',$code,PDO::PARAM_STR);
            $update->bindParam(':email',$email,PDO::PARAM_STR);
            $update->execute();
        }
        else {

            $sql = $this->database->connect()->prepare('
        insert into logged_users (email, code)
        values(?,?)
        ');
            $sql->execute([
                $email,
                $code
            ]);
        }
        setcookie('USER', json_encode(['email'=>$email,'code'=>$code]));

    }
    public function checkCookies()
    {
        if(isset($_COOKIE['USER']))
        {
            $stmt=$this->database->connect()->prepare('
            select code from logged_users where email=:email
            ');
            $stmt->bindParam(':email',json_decode($_COOKIE['USER'],true)['email'],PDO::PARAM_STR);
            $stmt->execute();
            $result=$stmt->fetch(PDO::FETCH_ASSOC);
            if(json_decode($_COOKIE['USER'],true)['code']===$result['code'])
                {
                    return true;
                }
                else
                {
                    return false;
                }

        }
    }
}