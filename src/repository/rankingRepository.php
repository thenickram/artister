<?php

require_once 'repository.php';
require_once  __DIR__.'/../models/ranking.php';
class rankingRepository extends repository
{
    public function getRanking():array
    {
        $result=[];
        $stmt=$this->database->connect()->prepare(
          'select username, points from users order by points'
        );
        $stmt->execute();
        $temp=$stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp as $item)
        {
            $result[]=new ranking(
                $item['username'],
                $item['points']
            );
        }
        return $result;
    }
}