<?php

require_once 'repository.php';
require_once  __DIR__.'/../models/artist.php';

class artistRepository extends repository
{

    public function  getArtist(int $id): artist
    {
        $stmt=$this->database->connect()->prepare('
        SELECT * from public.artist WHERE id=:id');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $artist=$stmt->fetch(PDO::FETCH_ASSOC);


        if($artist==false)
        {
            return false;
        }
        return new artist(
            $artist['name'],
            $artist['year_of_origin'],
            $artist['genre'],
            $artist['subgenre1'],
            $artist['subgenre2'],
            $artist['subgenre3'],
            $artist['wiki_url'],
            $artist['page_url']
        );
    }
    public function addArtist(artist $artist)
    {
        $stmt=$this->database->connect()->prepare(
            'insert into artists (name, year_of_origin, genre, subgenre1, subgenre2, subgenre3,
            wiki_url, page_url, added_by)
            Values (?,?,?,?,?,?,?,?,?)'
        );
        $id=$this->getUserId();

        $genre=$this->getGenre($artist->getGenre());
        $subgenre1=$this->getSubgenre($artist->getSubgenre1());
        $subgenre2=$this->getSubgenre($artist->getSubgenre2());
        $subgenre3=$this->getSubgenre($artist->getSubgenre3());

        $stmt->execute([
            $artist->getName(),
            $artist->getYearOfOrigin(),
            $genre,
            $subgenre1,
            $subgenre2,
            $subgenre3,
            $artist->getWikiUrl(),
            $artist->getPageUrl(),
            $id
        ]);

    }
    private function getGenre ($genre)
    {
        $stmt=$this->database->connect()->prepare('
       select id from music_genres
       where genre=:genre'
        );
        $stmt->bindParam(':genre',$genre,PDO::PARAM_STR);
        $stmt->execute();

        $result=$stmt->fetch(PDO::FETCH_ASSOC);

        if($result==false)
        {
            return null;
        }
        else
        {
            return $result['id'];
        }
    }
    private function getSubgenre ($subgenre)
    {
        $stmt=$this->database->connect()->prepare('
       select id from music_subgenres
       where subgenre=:subgenre'
        );
        $stmt->bindParam(':subgenre',$subgenre,PDO::PARAM_STR);
        $stmt->execute();

        $result=$stmt->fetch(PDO::FETCH_ASSOC);

        if($result==false)
        {
            return null;
        }
        else
        {
            return $result['id'];
        }
    }
    private function getUserId ()
    {
        $stmt=$this->database->connect()->prepare(
            'select id from users where email=:email'
        );
        $stmt->bindParam(':email',json_decode($_COOKIE['USER'],true)['email'],PDO::PARAM_STR);
        $stmt->execute();
        $result=$stmt->fetch(PDO::FETCH_ASSOC);

        if($result==false)
        {
            return null;
        }
        else
        {
            return $result['id'];
        }
    }
}