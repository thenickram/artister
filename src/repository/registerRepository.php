<?php

require_once 'repository.php';
require_once  __DIR__.'/../models/register.php';

class registerRepository extends repository
{
    public function checkEmail($email):bool
    {
        $stmt=$this->database->connect()->prepare('
        SELECT email FROM users 
        WHERE email=:email
        ');
        $stmt->bindParam(':email',$email,PDO::PARAM_STR);
        $stmt->execute();
        $result=$stmt->fetch(PDO::FETCH_ASSOC);

        if($result==true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function checkAccount($account):bool
    {
        $stmt=$this->database->connect()->prepare('
        SELECT email FROM users 
        WHERE username=:username
        ');
         $stmt->bindParam(':username',$account,PDO::PARAM_STR);
         $stmt->execute();
         $result=$stmt->fetch(PDO::FETCH_ASSOC);

        if($result==true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function checkPasswords($pass,$repPass):bool
    {
        if($pass===$repPass)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public function addUser(register $register)
    {
        $stmt=$this->database->connect()->prepare(
            'insert into users(
                  email,password,username)
                  VALUES (?,?,?)'
        );
        $stmt->execute([
            $register->getEmail(),
            $register->getPassword(),
            $register->getAccountName()
        ]);
    }
}