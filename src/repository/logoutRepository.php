<?php
require_once 'repository.php';

class logoutRepository extends repository
{
    public function deleteUser()
    {
        $stmt=$this->database->connect()->prepare('
        delete from logged_users
        where email=:email');
        $stmt->bindParam(':email',json_decode($_COOKIE['USER'],true)['email'],PDO::PARAM_STR);
        $stmt->execute();
    }
}