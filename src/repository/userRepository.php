<?php

require_once 'repository.php';
require_once  __DIR__.'/../models/user.php';

class userRepository extends  repository
{
    public function  getUser(string $email): ?user
    {
        $stmt=$this->database->connect()->prepare('
        SELECT * from public.users WHERE email=:email');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user=$stmt->fetch(PDO::FETCH_ASSOC);


        if($user==false)
        {
            return null;
        }
        return new user(
            $user['email'],
            $user['password']
        );
    }
    public function myLogin()
    {
        $stmt=$this->database->connect()->prepare('
        select username from users 
        where email=:email');
        $stmt->bindParam(':email',json_decode($_COOKIE['USER'],true)['email'],PDO::PARAM_STR);
        $stmt->execute();

        $user=$stmt->fetch(PDO::FETCH_ASSOC);

        echo $user['username'];
    }
}