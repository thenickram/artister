<?php

require_once 'repository.php';
class settingsRepository extends repository
{
    public function changeUsername($user):string
    {
        if(!$this->checkUser($user)) {
            $stmt = $this->database->connect()->prepare('
            update users set username=:username where email=:email');
            $stmt->bindParam(':username', $user, PDO::PARAM_STR);
            $stmt->bindParam(':email', json_decode($_COOKIE['USER'], true)['email'], PDO::PARAM_STR);
            $stmt->execute();
            return 'Username changed';
        }
        else
        {
            return 'User exists';
        }
    }
    public function changePassword($password,$repPass)
    {
        if(!$this->checkPass($password,$repPass))
        {
            $passwordhash=md5($password);
            $stmt=$this->database->connect()->prepare('
        update users set password=:password where email=:email');
            $stmt->bindParam(':password',$passwordhash,PDO::PARAM_STR);
            $stmt->bindParam(':email',json_decode($_COOKIE['USER'],true)['email'],PDO::PARAM_STR);
            $stmt->execute();
            return 'Password changed';
        }
        else
        {
            return 'Passwords dont match';
        }

    }
    private function checkUser($user):bool
    {
        $stmt=$this->database->connect()->prepare('
        SELECT email FROM users 
        WHERE username=:username
        ');
        $stmt->bindParam(':username',$user,PDO::PARAM_STR);
        $stmt->execute();
        $result=$stmt->fetch(PDO::FETCH_ASSOC);

        if($result==true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private function checkPass($password,$repeatpass):bool
    {
        if($password===$repeatpass)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

}