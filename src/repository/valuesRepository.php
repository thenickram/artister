<?php
require_once 'repository.php';

class valuesRepository extends repository
{
    public function getGenres()
    {
        $stmt=$this->database->connect()->prepare('
        select genre from music_genres');
        $stmt->execute();
        $res=$stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($res as $result)
        {
            echo '<option value="'.$result['genre'].'">'.$result['genre'].'</option>';
        }
    }
    public function getSubgenres($genre)
    {


        $database=new database();
        $sql=$database->connect()->prepare('
        select id from music_genres
        where genre=:genre');
        $sql->bindParam(':genre', $genre,PDO::PARAM_STR);
        $sql->execute();
        $result=$sql->fetch(PDO::FETCH_ASSOC);


        $stmt=$database->connect()->prepare('
        select subgenre from public.music_subgenres as ms
        join music_genres as mg
        on ms.genres_id=mg.id
        where ms.genres_id=:genre');
        $stmt->bindParam(':genre', $result['id'], PDO::PARAM_STR);
        $stmt->execute();

        $res=$stmt->fetchAll(PDO::FETCH_ASSOC);

        echo '<select name="subgenre1">';
        echo '<option value="">-------</option>';
        foreach ($res as $result)
        {
            echo '<option value="'.$result['subgenre'].'">'.$result['subgenre'].'</option>';
        }
        echo '</select>';

        echo '<select name="subgenre2">';
        echo '<option value="">-------</option>';
        foreach ($res as $result)
        {
            echo '<option value="'.$result['subgenre'].'">'.$result['subgenre'].'</option>';
        }
        echo '</select>';

        echo '<select name="subgenre3">';
        echo '<option value="">-------</option>';
        foreach ($res as $result)
        {
            echo '<option value="'.$result['subgenre'].'">'.$result['subgenre'].'</option>';
        }
        echo '</select>';

    }

    public static function getArtists($artits)
    {
        $database=new database();
        $sql=$database->connect()->prepare('
        SELECT id FROM public.music_genres WHERE genre=:genre
        ');
        $sql->bindParam(':genre', $artits,PDO::PARAM_STR);
        $sql->execute();
        $result=$sql->fetch(PDO::FETCH_ASSOC);
        $id = $result['id'];

        $sql=$database->connect()->prepare('
        SELECT name, year_of_origin, subgenre1, subgenre2, subgenre3, wiki_url, page_url 
        FROM public.artists 
        WHERE genre=:id
        AND is_accepted=true
        ');
        $sql->bindParam(':id', $id,PDO::PARAM_INT);
        $sql->execute();
        $resultArtist=$sql->fetchAll(PDO::FETCH_ASSOC);


        for($i = 0; $i<sizeof($resultArtist); $i++)
        {
            $sql=$database->connect()->prepare('
            SELECT subgenre FROM public.music_subgenres WHERE id=:id
            ');
            $sql->bindParam(':id', $resultArtist[$i]['subgenre1'],PDO::PARAM_INT);
            $sql->execute();
            $resultArtist[$i]['subgenre1'] = $sql->fetch(PDO::FETCH_ASSOC)['subgenre'];

            $sql=$database->connect()->prepare('
            SELECT subgenre FROM public.music_subgenres WHERE id=:id
            ');
            $sql->bindParam(':id', $resultArtist[$i]['subgenre2'],PDO::PARAM_INT);
            $sql->execute();
            $resultArtist[$i]['subgenre2'] = $sql->fetch(PDO::FETCH_ASSOC)['subgenre'];

            $sql=$database->connect()->prepare('
            SELECT subgenre FROM public.music_subgenres WHERE id=:id
            ');
            $sql->bindParam(':id', $resultArtist[$i]['subgenre3'],PDO::PARAM_INT);
            $sql->execute();
            $resultArtist[$i]['subgenre3'] = $sql->fetch(PDO::FETCH_ASSOC)['subgenre'];
        }

        for($i = 0; $i<sizeof($resultArtist); $i++)
        {
            echo '<div class="artist" onclick="showArtistDetails('.$i.')">';
            echo '<p>Name: '.$resultArtist[$i]['name'].'</p>';
            echo '<p>Year: '.$resultArtist[$i]['year_of_origin'].'</p>';
            echo '<div class="artist_deteils_for_js">';
            echo '<p>Subgenres:'.$resultArtist[$i]['subgenre1'].'</p>';
            echo '<p>'.$resultArtist[$i]['subgenre2'].'</p>';
            echo '<p>'.$resultArtist[$i]['subgenre3'].'</p>';
            echo '<p><a href="'.$resultArtist[$i]['wiki_url'].'"</a></p>';
            echo '<p><a href="'.$resultArtist[$i]['page_url'].'"Web page: </a></p>';
            echo '</div>';
            echo '</div>';
        }
    }
}
