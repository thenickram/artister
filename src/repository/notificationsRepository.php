<?php

require_once 'repository.php';
require_once  __DIR__.'/../models/notifications.php';

class notificationsRepository extends repository
{
    public function getNoti(): array
    {
        $result=[];
        $stmt=$this->database->connect()->prepare(
            'select message from notifications join 
        users on user_id=id
        where users.email=:email'
        );
        $stmt->bindParam(':email',json_decode($_COOKIE['USER'],true)['email'],PDO::PARAM_STR);
        $stmt->execute();

        $nots=$stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($nots as $notifs)
        {
            $result[]=new notifications(
                $notifs['message']
            );
        }
        return $result;
    }
}