<?php

require_once 'repository.php';
require_once  __DIR__.'/../models/artist.php';

class adminRepository extends repository
{
    public function checkIfAdmin():bool
    {
        $stmt=$this->database->connect()->prepare('
        select is_admin from users where email=:email');
        $stmt->bindParam(':email',json_decode($_COOKIE['USER'],true)['email'],PDO::PARAM_STR);
        $stmt->execute();
        $result=$stmt->fetch(PDO::FETCH_ASSOC);

        if($result['is_admin']==true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static function submitArtist($artist)
    {
        echo '<script>alert("Page only for and admins!!")</script>';
        $database=new database();
        $stmt=$database->connect()->prepare('
        update artists set is_accepted=true where name=:artist');
        $stmt->bindParam(':artist', $artist,PDO::PARAM_STR);
        $stmt->execute();
    }
    public static function declineArtist($artist)
    {
        $database=new database();
        $stmt=$database->connect()->prepare('
        delete from artists where name=:artist');
        $stmt->bindParam(':artist', $artist,PDO::PARAM_STR);
        $stmt->execute();
    }
    public function callArtists():array
    {
        $result=[];
        $stmt=$this->database->connect()->prepare('
        select * from artists where is_accepted=false order by id limit 1');
        $stmt->execute();
        $tmp=$stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($tmp as $item)
        {
            $result[]=new artist(
                $item['name'],
                $item['year_of_origin'],
                $this->getGenre($item['genre']),
                $this->getSubgenre($item['subgenre1']),
                $this->getSubgenre($item['subgenre2']),
                $this->getSubgenre($item['subgenre3']),
                $item['wiki_url'],
                $item['page_url']
            );
        }
        return $result;
    }
    private function getGenre($id):string
    {
        $stmt=$this->database->connect()->prepare('
        select genre from music_genres where id=:id');
        $stmt->bindParam(':id',$id,PDO::PARAM_INT);
        $stmt->execute();
        $result=$stmt->fetch(PDO::FETCH_ASSOC);
        if ($result==null)
        {
            return 'No genre selected';
        }
        return $result['genre'];
    }
    private function getSubgenre($id):string
    {
        $stmt=$this->database->connect()->prepare('
        select subgenre from music_subgenres where id=:id');
        $stmt->bindParam(':id',$id,PDO::PARAM_INT);
        $stmt->execute();
        $result=$stmt->fetch(PDO::FETCH_ASSOC);


        if ($result==null)
        {
            return 'No subgenre selected';
        }
        return $result['subgenre'];
    }
}