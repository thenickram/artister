<?php
class User
{
    private $email, $password;
    public function __construct(string $email, string $password)
    {
        $this->email=$email;
        $this->password=$password;
    }
    public function setEmail(string $email)
    {
        $this->email=$email;
    }
    public function setPassword(string $password)
    {
        $this->password=$password;
    }

    public function getEmail()
    {
        return $this->email;
    }
    public function getPassword()
    {
        return $this->password;
    }
}