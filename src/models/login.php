<?php


class login
{
    private $email, $code;

    public function __construct($email, $code)
    {
        $this->email = $email;
        $this->code = $code;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email): void
    {
        $this->email = $email;
    }
    public function getCode()
    {
        return $this->code;
    }
    public function setCode($code): void
    {
        $this->code = $code;
    }


}