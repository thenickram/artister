<?php


class artist
{
    private $name, $year_of_origin, $genre, $subgenre1, $subgenre2, $subgenre3, $wiki_url, $page_url;

    public function __construct($name, $year_of_origin, $genre, $subgenre1, $subgenre2, $subgenre3, $wiki_url, $page_url)
    {
        $this->name = $name;
        $this->year_of_origin = $year_of_origin;
        $this->genre = $genre;
        $this->subgenre1 = $subgenre1;
        $this->subgenre2 = $subgenre2;
        $this->subgenre3 = $subgenre3;
        $this->wiki_url = $wiki_url;
        $this->page_url = $page_url;
    }
    public function getName()
    {
        return $this->name;
    }
    public function setName($name): void
    {
        $this->name = $name;
    }
    public function getYearOfOrigin()
    {
        return $this->year_of_origin;
    }
    public function setYearOfOrigin($year_of_origin): void
    {
        $this->year_of_origin = $year_of_origin;
    }
    public function getGenre()
    {
        return $this->genre;
    }
    public function setGenre($genre): void
    {
        $this->genre = $genre;
    }
    public function getSubgenre1()
    {
        return $this->subgenre1;
    }
    public function setSubgenre1($subgenre1): void
    {
        $this->subgenre1 = $subgenre1;
    }
    public function getSubgenre2()
    {
        return $this->subgenre2;
    }
    public function setSubgenre2($subgenre2): void
    {
        $this->subgenre2 = $subgenre2;
    }
    public function getSubgenre3()
    {
        return $this->subgenre3;
    }
    public function setSubgenre3($subgenre3): void
    {
        $this->subgenre3 = $subgenre3;
    }
    public function getWikiUrl()
    {
        return $this->wiki_url;
    }
    public function setWikiUrl($wiki_url): void
    {
        $this->wiki_url = $wiki_url;
    }
    public function getPageUrl()
    {
        return $this->page_url;
    }
    public function setPageUrl($page_url): void
    {
        $this->page_url = $page_url;
    }


}