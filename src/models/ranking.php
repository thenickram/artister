<?php


class ranking
{
    private $username, $points;

    public function __construct($username, $points)
    {
        $this->username = $username;
        $this->points = $points;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username): void
    {
        $this->username = $username;
    }

    public function getPoints()
    {
        return $this->points;
    }

    public function setPoints($points): void
    {
        $this->points = $points;
    }


}