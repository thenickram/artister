<?php

class register
{
    private $email, $password, $passwordAgain, $accountName;

    public function __construct($email, $password, $passwordAgain, $accountName)
    {
        $this->email = $email;
        $this->password = $password;
        $this->passwordAgain = $passwordAgain;
        $this->accountName = $accountName;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email): void
    {
        $this->email = $email;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($password): void
    {
        $this->password = $password;
    }
    public function getPasswordAgain()
    {
        return $this->passwordAgain;
    }
    public function setPasswordAgain($passwordAgain): void
    {
        $this->passwordAgain = $passwordAgain;
    }
    public function getAccountName()
    {
        return $this->accountName;
    }
    public function setAccountName($accountName): void
    {
        $this->accountName = $accountName;
    }

}