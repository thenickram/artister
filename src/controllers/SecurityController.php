<?php
require_once 'AppController.php';
require_once __DIR__.'/../models/user.php';
require_once __DIR__.'/../repository/userRepository.php';
require_once __DIR__.'/../models/login.php';
require_once __DIR__.'/../repository/loginRepository.php';
require_once __DIR__.'/../repository/logoutRepository.php';

class SecurityController extends AppController
{
    public function login()
    {
        $userRep=new userRepository();
        $loginRep=new loginRepository();

        if($this->isPost())
        {
            return $this->render('login');
        }

        $email=$_POST["email"];
        $password=md5($_POST["password"]);
        if($email==null)
        {
            return $this->render('login',['messages' => ['Welcome']]);
        }
        $user=$userRep->getUser($email);

        if(!$user)
        {
            return $this->render('login',['messages' => ['User not found']]);
        }
        if($user->getEmail() !== $email)
        {
            return $this->render('login',['messages' => ['User not found']]);
        }
        if($user->getPassword() !== $password)
        {
            return $this->render('login',['messages' => ['Wrong password']]);
        }
        $loginRep->setCookies($email);
        return $this->render('main');
    }
    public function logout()
    {
        $logoutRep=new logoutRepository();
        $logoutRep->deleteUser();
        setcookie("USER","",time()-3600);
        return $this->render('login',['messages' => ['User logged out']]);
    }
}