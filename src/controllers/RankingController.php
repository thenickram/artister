<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/rankingRepository.php';

class RankingController extends AppController
{
    private $rankRepo;

    public function __construct()
    {
        $this->rankRepo=new rankingRepository();
    }

    public function ranking()
    {
        $ranks=$this->rankRepo->getRanking();
        $this->render('ranking',['ranks'=>$ranks]);
    }

}