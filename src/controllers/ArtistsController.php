<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/artist.php';
require_once __DIR__.'/../repository/artistRepository.php';

class ArtistController extends AppController
{
    private $message = [];
    private $artistRepository;

    public function __construct()
    {
        parent:: __constructor();
        $this->artistRepository=new artistRepository();
    }

    public function addArtist()
    {
        if ($this->isPost())
        {
            $artist=new artist(
                $_POST['name'],
                $_POST['year_of_origin'],
                $_POST['genre'],
                $_POST['subgenre1'],
                $_POST['subgenre2'],
                $_POST['subgenre3'],
                $_POST['wiki_url'],
                $_POST['page_url']
            );
            $this->artistRepository->addArtist($artist);
            $this->render('add_artist');
        }
    }
}

