<?php
require_once 'AppController.php';
require_once __DIR__.'/../repository/adminRepository.php';

class AdminController extends AppController
{
    public function adminPage()
    {
        $adminRep=new adminRepository();
        if($adminRep->checkIfAdmin())
        {
            $artist=$adminRep->callArtists();
            $this->render('adminPage',['artist'=>$artist]);
        }
        else
        {
            echo '<script>alert("Page only for and admins!!")</script>';
            $this->render('main');
        }
    }
}