<?php

require_once 'AppController.php';

class DefaultController extends AppController
{  
    public function index()
    {
        $this->render('login');
    }
    public function main_page()
    {
        $this->render('main');
    }
    public function register()
    {
        $this->render('register');
    }
    public function add_artist()
    {
        $this->render('add_artist');
    }

    public function settings()
    {
        $this->render('settings');
    }




}