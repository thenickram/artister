<?php
require_once 'AppController.php';
require_once __DIR__.'/../repository/settingsRepository.php';

class SettingsController extends AppController
{
    private $setRep;

    public function __construct()
    {
        $this->setRep = new settingsRepository();
    }

    public function changeAccount()
    {
        $password = $_POST['changePassword'];
        $repeatpassword = $_POST['changePasswordRepeat'];
        $accountname = $_POST['changeUsername'];
        $result = '';
        if ($accountname != NULL) {
            $result = $this->setRep->changeUsername($accountname);
        }
        if ($password != NULL) {
            $result += $this->setRep->changePassword($password, $repeatpassword);
        }
        $this->render('settings', ['messages' => [$result]]);
    }

}