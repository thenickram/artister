<?php
require_once 'AppController.php';
require_once __DIR__.'/../models/register.php';
require_once __DIR__.'/../repository/registerRepository.php';

class RegisterController extends AppController
{
    private $registerRepository;
    public function __construct()
    {
        parent:: __constructor();
        $this->registerRepository = new registerRepository();
    }

    public function addNewUser()
    {
        if($this->isPost())
        {
            $email=$_POST['email'];
            $password=$_POST['password'];
            $repeatpassword=$_POST['repeatpassword'];
            $accountname=$_POST['accountname'];

            $email=htmlentities($email,ENT_QUOTES,"UTF-8");
            $password=htmlentities($password,ENT_QUOTES,"UTF-8");
            $repeatpassword=htmlentities($repeatpassword,ENT_QUOTES,"UTF-8");
            $accountname=htmlentities($accountname,ENT_QUOTES,"UTF-8");

            $passwordHash=md5($password);
            $repPasswordHash=md5($repeatpassword);

            $user=new register(
                $email,
                $passwordHash,
                $repPasswordHash,
                $accountname
            );
            if($this->registerRepository->checkEmail($user->getEmail())==true)
            {
                $this->render('register', ['messages' => ['Email already used']]);
            }
            else if ($this->registerRepository->checkAccount($user->getAccountName())==true)
            {
                $this->render('register', ['messages' => ['Account already exist']]);
            }
            else if ($this->registerRepository->checkPasswords($user->getPassword(),$user->getPasswordAgain())==true)
            {
                $this->render('register', ['messages' => ['Passwords dont match']]);
            }
            else
            {
                $this->registerRepository->addUser($user);
                $this->render('login', ['messages'=>['User have been added']]);
            }
        }
    }
}