<?php
require_once 'AppController.php';
require_once __DIR__.'/../repository/notificationsRepository.php';

class NotificationsController extends AppController
{
    private $notiRep;

    public function __construct()
    {
        $this->notiRep=new notificationsRepository();
    }

    public function notifications()
    {
        $nots=$this->notiRep->getNoti();
        $this->render('notifications',['nots'=>$nots]);
    }
}