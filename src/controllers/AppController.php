<?php

class AppController
{
    private $request;
    public function __constructor()
    {
        $this->request=$_SERVER['REQUEST_METHOD'];
    }
    protected function isPost():bool
    {
        return $this->request === 'POST';
    }
    protected function isGet():bool
    {
        return $this->request === 'GET';
    }
    protected function render(string $template=null, array $variables=[])
    {
        $templatePath='public/views/'.$template.'.php';
        $output="File not found";

        if(file_exists($templatePath))
        {
            extract($variables);

            ob_start();
            include $templatePath;
            $output = ob_get_clean();
        }
        print $output;
    }
    protected function setCookies($email)
    {
        $code=$this->generateString();
    }
    private function generateString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 200; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}