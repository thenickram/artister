const form = document.querySelector("form");
const emailInput=form.querySelector('input[name="email"]');
const passwordInput=form.querySelector('input[name="password"]');

function isEmail(email)
{
    return /\S+@\S+.\S+/.test(email);
}
/*function isStrongPass(pass)
{
    let password=new regExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
    retrun password.test(pass);
}*/
function markValid(element, condition)
{
    !condition ? element.classList.add('no-valid') : element.classList.remove('no-valid');
}

emailInput.addEventListener('keyup',function ()
{
    setTimeout(function ()
    {
        markValid(emailInput,isEmail(emailInput.value))
    }
    ,1000);
});