<?php

require_once 'config.php';
class database
{
    private $user;
    private $password;
    private $host;
    private $database;


    public function __construct()
    {
        $this->user = USERNAME;
        $this->password = PASSWORD;
        $this->host = HOST;
        $this->database = DATABASE;
    }
    public function connect()
    {
        try
        {
        $conn=new PDO(
            "pgsql:host=$this->host;port=5432;dbname=$this->database",
            $this->user,
            $this->password,
            ["sslmode"  => "prefer"]
        );

        $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        return $conn;
        }catch (PDOException $e)
        {
            die('Connection failed: '.$e->getMessage());
        }
    }
}
