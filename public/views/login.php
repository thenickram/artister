<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <title>LOGIN</title> 
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
            <div class="sentence">
                Find yourself <br>a new artist <br>to listen to
            </div>
        </div> 
        
        <div class="login-container">
            <div class="messages">
                <?php
                    if(isset($messages))
                    {
                        foreach($messages as $message)
                        {
                            echo $message;
                        }
                    }
                ?>
            </div>
            <form class="login" action="login" method="POST">
                <input type="text" name="email" placeholder="email@email.com">
                <input type="password" name="password" placeholder="*********">
                <div class = "login-buttons">
                    <button type="submit">LOGIN</button>
                    <a href="register" class="register">REGISTER</a>
                </div>
            </form>
        </div>
    </div>
</body>