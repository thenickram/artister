<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../src/js/validateArtists.js"></script>
    <title>Admin Page</title>
</head>

<body>
<div class="base-container">
    <nav>
        <img src="public/img/logo.svg">
        <ul>
            <li>
                <a href="ranking" class="button">
                    <i class="fas fa-trophy"></i> Ranking</a>
            </li>
            <li>
                <a href="main_page" class="button">
                    <i class="fas fa-guitar"></i> Find artist</a>
            </li>
            <li>
                <a href="add_artist" class="button">
                    <i class="fas fa-plus-circle"></i> Add artist</a>
            </li>
            <li>
                <a href="notifications" class="button">
                    <i class="fas fa-bell"></i> Notifications</a>
            </li>
            <li>
                <a href="settings" class="button">
                    <i class="fas fa-cog"></i> Settings</a>
            </li>
            <li>
                <a href="adminPage" class="button">Submit Artists</a>
            </li>
        </ul>
    </nav>
    <main>
        <header>
            <div class="acc">
                <?php
                $user=new userRepository();
                $user->myLogin();
                ?>
            </div>
            <div class="logout">
                <a href="logout" class="logut button"> <i class="fas fa-sign-out-alt"></i> Logout</a>
            </div>
        </header>
        <div class="confrimArtists">
        <form method="POST" name="confrimArtists">
            <?php
            foreach($artist as $art):
                ?>
              <input type="text" value="<?=$art->getName()?>" class="artist_to_submit" name="artist_to_submit"
              style="display: none;">
                    <?= $art->getName() ?>
                    <?= $art->getYearOfOrigin() ?>
                    <?= $art->getGenre() ?>
                    <?= $art->getSubgenre1() ?>
                    <?= $art->getSubgenre2() ?>
                    <?= $art->getSubgenre3() ?>
                    <a href="<?= $art->getWikiUrl() ?>"> <?= $art->getWikiUrl() ?></a>
                    <a href="<?= $art->getPageUrl() ?>"> <?= $art->getPageUrl() ?></a>
                <?php
            endforeach; ?>
            <br>
            <button class="adminButton" onclick="confirmArtist()">Confirm</button>
            <button class="adminButton" onclick="declineArtist()">Decline</button>
        </form>
        </div>
    </main>
</div>
</body>
</html>