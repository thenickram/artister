<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <title>Settings</title>
</head>

<body>
    <div class="base-container">
    <nav>
            <img src="public/img/logo.svg">
        <ul>
            <li>
                <a href="ranking" class="button">
                    <i class="fas fa-trophy"></i> Ranking</a>
            </li>
            <li>
                <a href="main_page" class="button">
                    <i class="fas fa-guitar"></i> Find artist</a>
            </li>
            <li>
                <a href="add_artist" class="button">
                    <i class="fas fa-plus-circle"></i> Add artist</a>
            </li>
            <li>
                <a href="notifications" class="button">
                    <i class="fas fa-bell"></i> Notifications</a>
            </li>
            <li>
                <a href="settings" class="button">
                    <i class="fas fa-cog"></i> Settings</a>
            </li>
            <li>
                <a href="adminPage" class="button">Submit Artists</a>
            </li>
        </ul>
    </nav>
    <main>
        <header>
                <div class="acc">
                    <?php
                    $user=new userRepository();
                    $user->myLogin();
                    ?>
                </div>
                <div class="logout">
                    <a href="logout" class="logut button"> <i class="fas fa-sign-out-alt"></i> Logout</a>
                </div>
            </header>   
            <section class="myaccount">
                <div class="messages">
                    <?php
                    if(isset($messages))
                    {
                        foreach($messages as $message)
                        {
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <form action="changeAccount" class="changeAccount" method="POST">
                    <input type="text" name="changeUsername" placeholder="change username">
                    <input type="password" name="changePassword" placeholder="change password">
                    <input type="password" name="changePasswordRepeat" placeholder="repeat password">
                    <button type="submit" class="button">Change account info</button>
                </form>
            </section>
    </main>
    </div>
</body>