<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../src/js/findGenreDetails.js"></script>
    <title>Main Page</title>
</head>

<body>
    <div class="base-container">
    <nav>
            <img src="public/img/logo.svg">
            <ul>
                <li>
                    <a href="ranking" class="button"> 
                    <i class="fas fa-trophy"></i> Ranking</a>
                </li>
                <li>
                    <a href="main_page" class="button">
                    <i class="fas fa-guitar"></i> Find artist</a>
                </li>
                <li>
                    <a href="add_artist" class="button">
                    <i class="fas fa-plus-circle"></i> Add artist</a>
                </li>
                <li>
                    <a href="notifications" class="button">
                    <i class="fas fa-bell"></i> Notifications</a>
                </li>
                <li>
                    <a href="settings" class="button">
                    <i class="fas fa-cog"></i> Settings</a>
                </li>
                <li>
                    <a href="adminPage" class="button">Submit Artists</a>
                </li>
            </ul>
    </nav>
    <main>
        <header>
                <div class="acc">
                    <?php
                    $user=new userRepository();
                    $user->myLogin();
                    ?>
                </div>
                <div class="logout">
                    <a href="logout" class="logut button"> <i class="fas fa-sign-out-alt"></i> Logout</a>
                </div>
            </header>   
            <div class="find-artist">
                <form class="search">
                   Select genre <select class="genre" onchange="getGenreDetails()">
                        <?php
                        require_once 'src/repository/valuesRepository.php';
                        $list=new valuesRepository();
                        $list->getGenres();
                        ?>
                       </select>
                </form>
            </div>
        <div class="browse-artists">
        </div>
    </main>
    </div>
</body>