<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <script type="text/javascript" src="../src/js/registerValidation.js" defer></script>
   <title>REGISTER</title>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
            <div class="sentence">
                Find yourself <br>a new artist <br>to listen to
            </div>
        </div> 
        
        <div class="login-container">
            <div class="messages">
                <?php
                if(isset($messages))
                {
                    foreach($messages as $message)
                    {
                        echo $message;
                    }
                }
                ?>
            </div>
           <form class="register" method="post" action="addNewUser">
               <input type="text" name="email" placeholder="email">
               <input type="password" name="password" placeholder="********">
               <input type="password" name="repeatpassword" placeholder="********">
               <input type="text" name="accountname" placeholder="accountname">
               <button type="submit">Register</button>
               <a href="login">Go back</a>
            </form>
        </div>
    </div>
</body>
</html>