<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../src/js/genreChange.js"></script>
    <title>Add artist</title>
</head>

<body>
    <div class="base-container">
    <nav>
            <img src="public/img/logo.svg">
            <ul>
                <li>
                    <a href="ranking" class="button"> 
                    <i class="fas fa-trophy"></i> Ranking</a>
                </li>
                <li>
                    <a href="main_page" class="button">
                    <i class="fas fa-guitar"></i> Find artist</a>
                </li>
                <li>
                    <a href="add_artist" class="button">
                    <i class="fas fa-plus-circle"></i> Add artist</a>
                </li>
                <li>
                    <a href="notifications" class="button">
                    <i class="fas fa-bell"></i> Notifications</a>
                </li>
                <li>
                    <a href="settings" class="button">
                    <i class="fas fa-cog"></i> Settings</a>
                </li>
                <li>
                    <a href="adminPage" class="button">Submit Artists</a>
                </li>
            </ul>
    </nav>
    <main>
        <header>
                <div class="acc">
                    <?php
                        $user=new userRepository();
                        $user->myLogin();
                    ?>
                </div>
                <div class="logout">
                    <a href="logout" class="logut button"> <i class="fas fa-sign-out-alt"></i> Logout</a>
                </div>
            </header>   
            <div class="add-artist">

               <form class="add" action="addArtist" method="POST">
                   <div class="add-artist-select">
                    Select genre <select id="genre" name="genre" onchange="getSubgenres()">
                      <?php
                       require_once 'src/repository/valuesRepository.php';
                       $list=new valuesRepository();
                       $list->getGenres();
                       ?>
                   </select></br>
                </div>
                    <input type="text" name="name" placeholder="name">
                    <input type="text" name="year_of_origin" placeholder="year of origin">
                    <input type="text" name="wiki_url" placeholder="wikiurl">
                    <input type="text" name="page_url" placeholder="pageurl">
                    <button type="submit" class="button">Submit artist</button>
                </form>
            </div>
        <div class="add-artists">
            <form method="POST" action="" class="add-artists-form">
                
            </form>
        </div>
    </main>
    </div>
</body>
</html>