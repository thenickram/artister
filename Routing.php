<?php

require_once 'src/controllers/DefaultController.php';
require_once 'src/controllers/SecurityController.php';
require_once 'src/controllers/ArtistsController.php';
require_once 'src/controllers/RegisterController.php';
require_once 'src/controllers/NotificationsController.php';
require_once 'src/controllers/AdminController.php';
require_once 'src/controllers/RankingController.php';
require_once 'src/controllers/SettingsController.php';
require_once 'src/repository/loginRepository.php';

class Routing
{
    public static $routes;

    public static function get($url, $controller)
    {
        self::$routes[$url] = $controller;
    }
    public static function post($url, $controller)
    {
        self::$routes[$url] = $controller;
    }

    public static function run($url)
    {

        $loginRep=new loginRepository();
        if($loginRep->checkCookies()==false and $url !== 'register' and $url !== 'addNewUser')
        {
            $url='login';
        }
        $action = explode("/",$url)[0];

        if(!array_key_exists($action, self::$routes))
        {
            die("Wrong url");
        }

        $controller = self::$routes[$action];
        $object = new $controller;
        $action = $action ?: 'index';

        $object->$action();
    }
}