<?php

require 'Routing.php';
$path = trim($_SERVER['REQUEST_URI'],'/');
$path = parse_url($path,PHP_URL_PATH);

Routing::get('','DefaultController');
Routing::get('main_page','DefaultController');
Routing::get('register','DefaultController');
Routing::get('notifications','NotificationsController');
Routing::get('ranking','RankingController');
Routing::get('add_artist','DefaultController');
Routing::get('settings','DefaultController');
Routing::get('logout','SecurityController');
Routing::get('adminPage','AdminController');


Routing::post('login','SecurityController');
Routing::post('addArtist','ArtistController');
Routing::post('addNewUser','RegisterController');
Routing::post('changeAccount','SettingsController');
Routing::run($path);

?>